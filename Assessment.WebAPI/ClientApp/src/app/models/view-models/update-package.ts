import { Product } from "../product";

export class UpdatePackageViewModel {
  id: string;
  name: string;
  description: string;
  products: Product[];

  constructor() {
    this.products = [];
  }
}