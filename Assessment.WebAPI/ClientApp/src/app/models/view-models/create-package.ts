export class CreatePackageViewModel {
  name: string;
  description: string;
  products: string[];

  constructor() {
    this.products = [];
  }
}