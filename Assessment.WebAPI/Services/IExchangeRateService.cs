﻿using Assessment.WebAPI.Models.ViewModels;
using System.Threading.Tasks;

namespace Assessment.WebAPI.Services
{
    public interface IExchangeRateService
    {
        Task<ExchangeRatesApiResponse> GetExchangeRate(string currency);
    }
}
