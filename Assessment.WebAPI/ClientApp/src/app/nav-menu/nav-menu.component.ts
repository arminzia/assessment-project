import { Component } from "@angular/core";
import { CartStoreService } from "../services/cart-store.service";

@Component({
  selector: "app-nav-menu",
  templateUrl: "./nav-menu.component.html",
  styleUrls: ["./nav-menu.component.scss"]
})
export class NavMenuComponent {
  isExpanded = false;

  constructor(public cartStore: CartStoreService) {}

  collapse() {
    this.isExpanded = false;
  }

  toggle() {
    this.isExpanded = !this.isExpanded;
  }
}
