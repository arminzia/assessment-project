﻿using Assessment.WebAPI.ActionFilters;
using Assessment.WebAPI.Models;
using Assessment.WebAPI.Models.Entities;
using Assessment.WebAPI.Models.Requests;
using Assessment.WebAPI.Models.ViewModels;
using Assessment.WebAPI.Services;
using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Assessment.WebAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class PackagesController : ControllerBase
    {
        private readonly ILogger<PackagesController> _logger;
        private readonly AssessmentContext _dbContext;
        private readonly IMapper _mapper;
        private readonly IExchangeRateService _exchangeRateService;

        public PackagesController(
            ILogger<PackagesController> logger,
            AssessmentContext dbContext,
            IMapper mapper,
            IExchangeRateService exchangeRateService)
        {
            _logger = logger;
            _dbContext = dbContext;
            _mapper = mapper;
            _exchangeRateService = exchangeRateService;
        }

        [HttpGet]
        public async Task<IActionResult> Get(string currency)
        {
            var packages = await _dbContext.Packages
                .Include(x => x.Products)
                .ThenInclude(x => x.Product)
                .ToListAsync();

            var result = _mapper.Map<Package[], IEnumerable<PackageViewModel>>(packages.ToArray());

            // apply exchange rate
            if (!string.IsNullOrEmpty(currency))
            {
                var exchangeRateResponse = await _exchangeRateService.GetExchangeRate(currency);
                var exchangeRate = exchangeRateResponse.Rates.FirstOrDefault();

                // update each package
                foreach (var package in result)
                {
                    var exchangedPrice = package.Price * exchangeRate.Value;

                    package.Price = exchangedPrice;
                    package.Currency = currency.ToUpper();
                }
            }

            return Ok(result);
        }

        [HttpGet("{id:guid}")]
        public async Task<IActionResult> Get(Guid id, string currency)
        {
            if (id == null || id == Guid.Empty)
                return BadRequest("package is required");

            var package = await _dbContext.Packages
                .Include(x => x.Products)
                .ThenInclude(x => x.Product)
                .SingleOrDefaultAsync(x => x.Id == id);

            if (package == null)
                return NotFound();

            var result = _mapper.Map<PackageViewModel>(package);

            // apply exchange rate
            if (!string.IsNullOrEmpty(currency))
            {
                var exchangeRateResponse = await _exchangeRateService.GetExchangeRate(currency);
                var exchangeRate = exchangeRateResponse.Rates.FirstOrDefault();

                var exchangedPrice = result.Price * exchangeRate.Value;

                result.Price = exchangedPrice;
                result.Currency = currency.ToUpper();
            }

            return Ok(result);
        }

        [HttpPost]
        [ValidateModel]
        public async Task<IActionResult> Post(CreatePackageModel model)
        {
            var package = new Package
            {
                Id = Guid.NewGuid(),
                Name = model.Name,
                Description = model.Description
            };

            foreach (var productId in model.Products)
            {
                package.Products.Add(new PackageItem
                {
                    ProductId = productId
                });
            }

            await _dbContext.Packages.AddAsync(package);
            await _dbContext.SaveChangesAsync();

            //var record = await _dbContext.Packages
            //    .Include(x => x.Products)
            //    .ThenInclude(x => x.Product)
            //    .SingleOrDefaultAsync(x => x.Id == package.Id);

            //var result = _mapper.Map<PackageViewModel>(record);

            return Ok();
        }

        [HttpPut("{id:guid}")]
        [ValidateModel]
        public async Task<IActionResult> Put(Guid id, UpdatePackageModel model)
        {
            if (id == null || id == Guid.Empty)
                return BadRequest("package id is required");

            var package = await _dbContext.Packages
                .Include(x => x.Products)
                .ThenInclude(x => x.Product)
                .SingleOrDefaultAsync(x => x.Id == id);

            if (package == null)
                return NotFound();

            package.Name = model.Name;
            package.Description = model.Description;

            // process added products
            foreach (var productId in model.Products)
            {
                if (!package.Products.Any(x => x.ProductId == productId))
                {
                    package.Products.Add(new PackageItem
                    {
                        ProductId = productId
                    });
                }
            }

            // process removed products
            foreach (var item in package.Products.ToList())
            {
                if (!model.Products.Any(x => x == item.ProductId))
                    package.Products.Remove(item);
            }

            await _dbContext.SaveChangesAsync();

            //var record = await _dbContext.Packages
            //    .Include(x => x.Products)
            //    .ThenInclude(x => x.Product)
            //    .SingleOrDefaultAsync(x => x.Id == id);

            //var result = _mapper.Map<PackageViewModel>(record);

            return Ok();
        }

        [HttpDelete("{id:guid}")]
        public async Task<IActionResult> Delete(Guid id)
        {
            if (id == null || id == Guid.Empty)
                return BadRequest("package is is required");

            var package = await _dbContext.Packages.FindAsync(id);
            if (package == null)
                return NotFound();

            _dbContext.Packages.Remove(package);
            await _dbContext.SaveChangesAsync();

            return Ok();
        }
    }
}