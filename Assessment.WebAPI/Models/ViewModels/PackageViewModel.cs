﻿using System.Collections.Generic;

namespace Assessment.WebAPI.Models.ViewModels
{
    public class PackageViewModel
    {
        public string Id { get; set; }

        public string Name { get; set; }

        public string Description { get; set; }

        public List<ProductViewModel> Products { get; set; }

        public decimal? Price { get; set; }
        
        public string Currency { get; set; } = "USD";
    }
}
