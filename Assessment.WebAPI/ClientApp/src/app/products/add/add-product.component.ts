import { NgForm } from "@angular/forms";
import { Component, OnInit, ViewChild } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { ToastrService } from "ngx-toastr";
import { environment } from "src/environments/environment";
import { Product } from "src/app/models/product";

@Component({
  selector: "app-add-product",
  templateUrl: "./add-product.component.html"
})
export class AddProductComponent implements OnInit {
  product: Product;

  @ViewChild("productForm", { static: false }) productForm: NgForm;

  constructor(private http: HttpClient, private toastr: ToastrService) {
    this.product = new Product();
  }

  ngOnInit() {}

  onSubmit() {
    const url = `${environment.apiUrl}/products`;
    this.http.post(url, this.product).subscribe((resp: Product) => {
      this.toastr.success("Product created 🥂");
      this.productForm.reset();
    });
  }
}
