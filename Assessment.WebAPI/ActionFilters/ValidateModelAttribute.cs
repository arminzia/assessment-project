﻿using Assessment.WebAPI.Models.Validation;
using Microsoft.AspNetCore.Mvc.Filters;

namespace Assessment.WebAPI.ActionFilters
{
    public sealed class ValidateModelAttribute : ActionFilterAttribute
    {
        public override void OnActionExecuting(ActionExecutingContext context)
        {
            if (!context.ModelState.IsValid)
            {
                context.Result = new ValidationFailedResult(context.ModelState);
            }
        }
    }
}
