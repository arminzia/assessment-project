import { Component, OnInit } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { ActivatedRoute } from "@angular/router";
import { ToastrService } from "ngx-toastr";
import { environment } from "src/environments/environment";
import { CartStoreService } from "src/app/services/cart-store.service";
import { Package } from "src/app/models/package";

@Component({
  selector: "app-package-details",
  templateUrl: "./package-details.component.html"
})
export class PackageDetailsComponent implements OnInit {
  packageId: string;
  package: Package;
  currency: string = "USD";
  addedToCart: boolean = false;

  constructor(
    private route: ActivatedRoute,
    private http: HttpClient,
    public cartStore: CartStoreService,
    private toastr: ToastrService
  ) {}

  ngOnInit() {
    const id = this.route.snapshot.paramMap.get("id");
    if (id != null) {
      this.packageId = id;

      const url = `${environment.apiUrl}/packages/${id}`;
      this.http.get(url).subscribe((resp: Package) => {
        this.package = resp;
      });
    }
  }

  apply() {
    const url = `${environment.apiUrl}/packages/${this.packageId}?currency=${this.currency}`;
    this.http.get(url).subscribe((resp: Package) => {
      this.package = resp;
    });
  }

  addToCart() {
    const existingPackage = this.cartStore.packages.find(item => item.id == this.package.id);

    if (existingPackage !== undefined) {
      this.toastr.error("Already in your cart 🤦‍♂️");
    } else {
      this.cartStore.addPackage(this.package);
      this.addedToCart = true;
      this.toastr.success("Package added to your cart 🤗", this.package.name);
    }
  }
}
