﻿using Assessment.WebAPI.Models;
using Assessment.WebAPI.Models.Entities;
using Assessment.WebAPI.Models.Requests;
using Assessment.WebAPI.Models.ViewModels;
using Assessment.WebAPI.Services;
using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Assessment.WebAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ProductsController : ControllerBase
    {
        private readonly ILogger<ProductsController> _logger;
        private readonly AssessmentContext _dbContext;
        private readonly IMapper _mapper;
        private readonly IExchangeRateService _exchangeRateService;

        public ProductsController(
            ILogger<ProductsController> logger,
            AssessmentContext dbContext,
            IMapper mapper,
            IExchangeRateService exchangeRateService)
        {
            _logger = logger;
            _dbContext = dbContext;
            _mapper = mapper;
            _exchangeRateService = exchangeRateService;
        }

        [HttpGet]
        public async Task<IActionResult> Get(string currency)
        {
            var products = await _dbContext.Products.ToListAsync();
            var result = _mapper.Map<Product[], IEnumerable<ProductViewModel>>(products.ToArray());

            // apply exchange rate
            if (!string.IsNullOrEmpty(currency))
            {
                var exchangeRateResponse = await _exchangeRateService.GetExchangeRate(currency);
                var exchangeRate = exchangeRateResponse.Rates.FirstOrDefault();

                foreach (var product in result)
                {
                    var exchangedPrice = product.Price * exchangeRate.Value;
                    product.Price = exchangedPrice;
                    product.Currency = currency.ToUpper();
                }
            }

            return Ok(result);
        }

        [HttpPost]
        public async Task<IActionResult> Post(CreateProductModel model)
        {
            var product = new Product
            {
                Id = Guid.NewGuid(),
                Name = model.Name,
                Price = model.Price
            };

            await _dbContext.AddAsync(product);
            await _dbContext.SaveChangesAsync();

            return Ok();
        }
    }
}