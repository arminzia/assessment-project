﻿using Assessment.WebAPI.Models.Entities;
using Assessment.WebAPI.Models.ViewModels;
using AutoMapper;

namespace Assessment.WebAPI.Models.AutoMapperProfiles
{
    public class PackageProfile : Profile
    {
        public PackageProfile()
        {
            CreateMap<Package, PackageViewModel>()
                .ForMember(dest => dest.Products, opt => opt.MapFrom<PackageResolver>());
        }
    }
}
