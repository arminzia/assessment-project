﻿using System;

namespace Assessment.WebAPI.Models.Entities
{
    public class BaseEntity
    {
        public Guid Id { get; set; }
    }
}
