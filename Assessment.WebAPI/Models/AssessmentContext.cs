﻿using Assessment.WebAPI.Models.Entities;
using Assessment.WebAPI.Models.EntityConfig;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Assessment.WebAPI.Models
{
    public class AssessmentContext : DbContext
    {
        public DbSet<Product> Products { get; set; }

        public DbSet<Package> Packages { get; set; }

        public AssessmentContext() { }

        public AssessmentContext(DbContextOptions options) : base(options) { }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            // this prevents configuring the DbContext more than once.
            if (!optionsBuilder.IsConfigured)
            {
                optionsBuilder.UseSqlServer(@"Data Source=.\\SQLEXPRESS;Initial Catalog=AssessmentContext;Integrated security=true;MultipleActiveResultSets=True;");
            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            modelBuilder.ApplyConfiguration(new ProductConfig());
            modelBuilder.ApplyConfiguration(new PackageConfig());
            modelBuilder.ApplyConfiguration(new PackageItemConfig());
        }
    }
}
