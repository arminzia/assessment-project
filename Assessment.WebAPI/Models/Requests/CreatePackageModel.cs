﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Assessment.WebAPI.Models.Requests
{
    public class CreatePackageModel
    {
        [Required]
        [StringLength(50)]
        public string Name { get; set; }

        [StringLength(500)]
        public string Description { get; set; }

        public List<Guid> Products { get; set; }

        public CreatePackageModel()
        {
            Products = new List<Guid>();
        }
    }
}
