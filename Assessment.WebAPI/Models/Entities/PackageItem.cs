﻿using System;
using System.Text.Json.Serialization;

namespace Assessment.WebAPI.Models.Entities
{
    public class PackageItem : BaseEntity
    {
        [JsonIgnore]
        public Guid PackageId { get; set; }
        
        [JsonIgnore]
        public virtual Package Package { get; set; }

        [JsonIgnore]
        public Guid ProductId { get; set; }

        public virtual Product Product { get; set; }
    }
}
