import { Component, OnInit } from "@angular/core";
import { CartStoreService } from "../services/cart-store.service";

@Component({
  selector: "app-cart",
  templateUrl: "./cart.component.html"
})
export class CartComponent implements OnInit {
  constructor(public cartStore: CartStoreService) {}

  ngOnInit() {}

  remove(packageId: string) {
    if (confirm("Are you sure you want to remove this from your cart?")) {
      this.cartStore.removePackage(packageId);
    }
  }
}
