import { Component, OnInit } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { Package } from "../models/package";
import { ToastrService } from "ngx-toastr";

import { environment } from "src/environments/environment";
import { CartStoreService } from "../services/cart-store.service";

@Component({
  selector: "app-packages",
  templateUrl: "./packages.component.html"
})
export class PackagesComponent implements OnInit {
  packages: Package[];
  currency: string = "USD";
  searchText: string;

  constructor(
    private http: HttpClient,
    private toastr: ToastrService,
    public cartStore: CartStoreService
  ) {
    this.packages = [];
  }

  ngOnInit() {
    const url = `${environment.apiUrl}/packages`;
    this.http.get(url).subscribe((data: Package[]) => {
      this.packages = data;
    });
  }

  remove(pkg: Package) {
    if (confirm("Are you sure you want to delete this package?")) {
      const url = `${environment.apiUrl}/packages/${pkg.id}`;
      this.http.delete(url).subscribe(resp => {
        const index = this.packages.indexOf(pkg, 0);
        this.packages.splice(index, 1);
        this.toastr.success("Package deleted 👐");
      });
    }
  }

  apply() {
    const url = `${environment.apiUrl}/packages/?currency=${this.currency}`;
    this.http.get(url).subscribe((resp: Package[]) => {
      this.packages = resp;
    });
  }

  addToCart(pkg: Package) {
    const existingPackage = this.cartStore.packages.find(item => item.id == pkg.id);

    if (existingPackage !== undefined) {
      this.toastr.error("Already in your cart 🤦‍♂️");
    } else {
      this.cartStore.addPackage(pkg);
      this.toastr.success("Package added to your cart 🤗", pkg.name);
    }
  }
}
