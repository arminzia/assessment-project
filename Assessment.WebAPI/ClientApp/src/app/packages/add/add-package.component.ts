import { NgForm } from "@angular/forms";
import { Component, OnInit, ViewChild } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { ToastrService } from "ngx-toastr";
import { environment } from "src/environments/environment";
import { Product } from "src/app/models/product";
import { Package } from "src/app/models/package";
import { CreatePackageViewModel } from "src/app/models/view-models/create-package";

@Component({
  selector: "app-add-package",
  templateUrl: "./add-package.component.html"
})
export class AddPackageComponent implements OnInit {
  package: CreatePackageViewModel;
  products: Product[];
  selectedProduct: Product;
  selectedProducts: Product[];

  @ViewChild("packageForm", { static: false }) packageForm: NgForm;

  constructor(private http: HttpClient, private toastr: ToastrService) {
    this.package = new CreatePackageViewModel();
    this.products = [];
    this.selectedProducts = [];
  }

  ngOnInit() {
    const url = `${environment.apiUrl}/products`;
    this.http.get(url).subscribe((data: Product[]) => {
      this.products = data;
    });
  }

  addProduct() {
    if (!this.selectedProduct) {
      this.toastr.error("Please select a product first");
      return;
    }

    const index = this.selectedProducts.indexOf(this.selectedProduct);
    if (index != -1) {
      this.toastr.warning("Duplicate product 🤦‍♂️");
      return;
    }

    this.selectedProducts.push(this.selectedProduct);
  }

  onSubmit() {
    const url = `${environment.apiUrl}/packages`;

    this.selectedProducts.forEach(product => {
      this.package.products.push(product.id);
    });

    this.http.post(url, this.package).subscribe((resp: Package) => {
      this.packageForm.reset();
      this.selectedProducts = [];

      this.toastr.success("Package created 🥂");
    });
  }
}
