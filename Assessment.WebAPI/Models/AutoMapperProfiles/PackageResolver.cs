﻿using Assessment.WebAPI.Models.Entities;
using Assessment.WebAPI.Models.ViewModels;
using AutoMapper;
using System.Collections.Generic;

namespace Assessment.WebAPI.Models.AutoMapperProfiles
{
    public class PackageResolver : IValueResolver<Package, PackageViewModel, List<ProductViewModel>>
    {
        public List<ProductViewModel> Resolve(Package source, PackageViewModel destination, List<ProductViewModel> destMember, ResolutionContext context)
        {
            var products = new List<ProductViewModel>();

            foreach (var item in source.Products)
            {
                products.Add(new ProductViewModel
                {
                    Id = item.ProductId.ToString(),
                    Name = item.Product.Name,
                    Price = item.Product.Price
                });
            }

            return products;
        }
    }
}
