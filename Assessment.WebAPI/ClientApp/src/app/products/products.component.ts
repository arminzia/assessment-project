import { Component, OnInit } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { environment } from "src/environments/environment";
import { Product } from "../models/product";

@Component({
  selector: "app-home",
  templateUrl: "./products.component.html"
})
export class ProductsComponent implements OnInit {
  products: Product[];
  currency: string = 'USD';
  searchText: string;

  constructor(private http: HttpClient) {
    this.products = [];
  }

  ngOnInit() {
    const url = `${environment.apiUrl}/products`;
    this.http.get(url).subscribe((data: Product[]) => {
      this.products = data;
    });
  }

  apply() {
    const url = `${environment.apiUrl}/products/?currency=${this.currency}`;
    this.http.get(url).subscribe((resp: Product[]) => {
      this.products = resp;
    });
  }
}
