﻿using Assessment.WebAPI.Models.ViewModels;
using RestSharp;
using System.Threading.Tasks;

namespace Assessment.WebAPI.Services
{
    public class ExchangeRateService : IExchangeRateService
    {
        public async Task<ExchangeRatesApiResponse> GetExchangeRate(string currency)
        {
            string symbol = currency.ToUpper();
            string baseUrl = $"https://api.exchangeratesapi.io/latest?base=USD&symbols={symbol}";

            RestClient client = new RestClient(baseUrl);
            RestRequest request = new RestRequest(baseUrl, Method.GET, DataFormat.Json);

            IRestResponse<ExchangeRatesApiResponse> response =
                await client.ExecuteAsync<ExchangeRatesApiResponse>(request);

            if (response.IsSuccessful)
                return response.Data;

            return null;
        }
    }
}
