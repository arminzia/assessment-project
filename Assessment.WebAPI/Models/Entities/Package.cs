﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;

namespace Assessment.WebAPI.Models.Entities
{
    public class Package : BaseEntity
    {
        public string Name { get; set; }

        public string Description { get; set; }

        public virtual List<PackageItem> Products { get; set; }

        public Package()
        {
            Products = new List<PackageItem>();
        }

        [NotMapped]
        public decimal? Price
        {
            get
            {
                return Products.Sum(x => x.Product?.Price);
            }
        }
    }
}
