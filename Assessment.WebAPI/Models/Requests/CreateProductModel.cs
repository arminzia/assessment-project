﻿using System.ComponentModel.DataAnnotations;

namespace Assessment.WebAPI.Models.Requests
{
    public class CreateProductModel
    {
        [Required]
        [StringLength(50)]
        public string Name { get; set; }

        [Required]
        public decimal Price { get; set; }
    }
}
