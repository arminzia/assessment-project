﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Assessment.WebAPI.Migrations
{
    public partial class InitialCreate : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Packages",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    Name = table.Column<string>(maxLength: 50, nullable: false),
                    Description = table.Column<string>(maxLength: 500, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Packages", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Products",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    Name = table.Column<string>(maxLength: 50, nullable: false),
                    Price = table.Column<decimal>(type: "decimal(18,4)", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Products", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "PackageItems",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    PackageId = table.Column<Guid>(nullable: false),
                    ProductId = table.Column<Guid>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_PackageItems", x => x.Id);
                    table.ForeignKey(
                        name: "FK_PackageItems_Packages_PackageId",
                        column: x => x.PackageId,
                        principalTable: "Packages",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_PackageItems_Products_ProductId",
                        column: x => x.ProductId,
                        principalTable: "Products",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Name", "Price" },
                values: new object[,]
                {
                    { new Guid("59a6b72c-d8fe-491f-bc93-516bd478fc9f"), "Shield", 1149m },
                    { new Guid("00d1b7e0-27d9-41c2-a65b-ff03b78c963d"), "Helmet", 999m },
                    { new Guid("e7711757-3a86-4974-88fd-c80117b43af4"), "Sword", 899m },
                    { new Guid("e18a8d99-2659-40d3-b0c8-7bea40cd69a0"), "Axe", 799m },
                    { new Guid("df769c14-3660-4989-81d5-fef0f4c34cbe"), "Knife", 349m },
                    { new Guid("cd53dbb6-a304-4257-a8eb-128ad3fcc370"), "Bow", 649m },
                    { new Guid("32394530-3a21-4653-8ba7-f61b3a21f088"), "Gold Coin", 249m },
                    { new Guid("fc449a79-916a-4c69-b72c-56144f0d74a2"), "Platinum Coin", 399m },
                    { new Guid("8baf8fba-8235-4b62-b97a-5ecf7c17692c"), "Silver Coin", 50m }
                });

            migrationBuilder.CreateIndex(
                name: "IX_PackageItems_PackageId",
                table: "PackageItems",
                column: "PackageId");

            migrationBuilder.CreateIndex(
                name: "IX_PackageItems_ProductId",
                table: "PackageItems",
                column: "ProductId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "PackageItems");

            migrationBuilder.DropTable(
                name: "Packages");

            migrationBuilder.DropTable(
                name: "Products");
        }
    }
}
