import { NgForm } from "@angular/forms";
import { Component, OnInit, ViewChild } from "@angular/core";
import { ActivatedRoute, Router } from "@angular/router";
import { HttpClient } from "@angular/common/http";
import { ToastrService } from "ngx-toastr";
import { environment } from "src/environments/environment";
import { Package } from "src/app/models/package";
import { Product } from "src/app/models/product";
import { UpdatePackageViewModel } from "src/app/models/view-models/update-package";

@Component({
  selector: "app-edit-package",
  templateUrl: "./edit-package.component.html"
})
export class EditPackageComponent implements OnInit {
  @ViewChild("packageForm", { static: false }) packageForm: NgForm;

  viewModel: UpdatePackageViewModel;
  products: Product[];
  selectedProduct: Product;

  constructor(
    private http: HttpClient,
    private toastr: ToastrService,
    private route: ActivatedRoute,
    private router: Router
  ) {
    this.viewModel = new UpdatePackageViewModel();
    this.products = [];
  }

  ngOnInit() {
    const id = this.route.snapshot.paramMap.get("id");
    this.loadPackage(id);
    this.loadProducts();
  }

  loadPackage(id: string) {
    const packageUrl = `${environment.apiUrl}/packages/${id}`;
    this.http.get(packageUrl).subscribe((data: Package) => {
      let viewModel = new UpdatePackageViewModel();
      viewModel.id = data.id;
      viewModel.name = data.name;
      viewModel.description = data.description;
      viewModel.products = data.products;
      this.viewModel = viewModel;
    });
  }

  loadProducts() {
    const url = `${environment.apiUrl}/products`;
    this.http.get(url).subscribe((data: Product[]) => {
      this.products = data;
    });
  }

  addProduct() {
    if (!this.selectedProduct) {
      this.toastr.error("Please select a product first");
      return;
    }

    const found = this.viewModel.products.filter(prod => {
      return prod.id == this.selectedProduct.id;
    }).length > 0;

    if (found) {
      this.toastr.warning("Duplicate product 🤦‍♂️");
      return;
    }

    this.viewModel.products.push(this.selectedProduct);
  }

  removeProduct(product: Product) {
    if (confirm("Are you sure you want to remove this?")) {
      const index = this.viewModel.products.indexOf(product, 0);
      this.viewModel.products.splice(index, 1);
    }
  }

  onSubmit() {
    let payload = {
      name: this.viewModel.name,
      description: this.viewModel.description,
      products: this.viewModel.products.map(p => { return p.id; })
    };

    const url = `${environment.apiUrl}/packages/${this.viewModel.id}`;
    this.http.put(url, payload).subscribe((resp: Package) => {
      this.toastr.success("Package updated 😌");
      this.router.navigateByUrl("/packages");
    });
  }
}
