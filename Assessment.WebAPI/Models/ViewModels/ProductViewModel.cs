﻿namespace Assessment.WebAPI.Models.ViewModels
{
    public class ProductViewModel
    {
        public string Id { get; set; }

        public string Name { get; set; }

        public decimal Price { get; set; }
        
        public string Currency { get; set; } = "USD";
    }
}
