import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { FormsModule } from "@angular/forms";
import { Routes, RouterModule } from "@angular/router";

import { NgbModule } from "@ng-bootstrap/ng-bootstrap";
import { NgProgressModule } from "@ngx-progressbar/core";
import { ToastrModule } from "ngx-toastr";

import { CartComponent } from "./cart/cart.component";
import { ProductsComponent } from "./products/products.component";
import { AddProductComponent } from "./products/add/add-product.component";

import { PackagesComponent } from "./packages/packages.component";
import { PackageDetailsComponent } from "./packages/details/package-details.component";
import { AddPackageComponent } from "./packages/add/add-package.component";
import { EditPackageComponent } from "./packages/edit/edit-package.component";

import { FilterPipe } from "./pipes/filter.pipe";

const routes: Routes = [
  { path: "", pathMatch: "full", redirectTo: "/packages" },
  { path: "products", component: ProductsComponent },
  { path: "products/add", component: AddProductComponent },
  { path: "packages", component: PackagesComponent },
  { path: "packages/details/:id", component: PackageDetailsComponent },
  { path: "packages/add", component: AddPackageComponent },
  { path: "packages/edit/:id", component: EditPackageComponent },
  { path: "cart", component: CartComponent }
];

@NgModule({
  declarations: [
    ProductsComponent,
    AddProductComponent,
    PackagesComponent,
    PackageDetailsComponent,
    AddPackageComponent,
    EditPackageComponent,
    CartComponent,
    FilterPipe
  ],
  imports: [
    CommonModule,
    FormsModule,
    RouterModule.forRoot(routes, { useHash: true, enableTracing: false }),
    NgbModule,
    NgProgressModule.withConfig({
      spinner: false
    }),
    ToastrModule.forRoot({
      positionClass: "toast-bottom-right",
      preventDuplicates: false
    })
  ],
  exports: [
    RouterModule,
    FormsModule,
    NgbModule,
    NgProgressModule,
    ToastrModule
  ]
})
export class AppRoutingModule {}
