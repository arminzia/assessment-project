import { Injectable } from "@angular/core";
import { BehaviorSubject } from "rxjs";
import { Package } from "../models/package";

@Injectable({ providedIn: 'root' })
export class CartStoreService {
  private readonly _packages = new BehaviorSubject<Package[]>([]);

  readonly packages$ = this._packages.asObservable();

  get packages(): Package[] {
    return this._packages.getValue();
  }

  get hasDiscount(): boolean {
    return this._packages.getValue().length > 1;
  }

  get totalPrice(): number {
    let price = 0;

    this._packages.getValue().forEach(item => {
      price += item.price;
    });

    return price;
  }

  get discountPrice(): number {
    const discount = (this.totalPrice * 10) / 100;
    return this.totalPrice - discount;
  }

  set packages(val: Package[]) {
    this._packages.next(val);
  }

  addPackage(pkg: Package) {
    this.packages = [
      ...this.packages,
      pkg
    ];
  }

  removePackage(id: string) {
    this.packages = this.packages.filter(pkg => pkg.id !== id);
  }
}