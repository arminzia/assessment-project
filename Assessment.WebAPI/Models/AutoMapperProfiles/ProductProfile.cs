﻿using Assessment.WebAPI.Models.Entities;
using Assessment.WebAPI.Models.ViewModels;
using AutoMapper;

namespace Assessment.WebAPI.Models.AutoMapperProfiles
{
    public class ProductProfile : Profile
    {
        public ProductProfile()
        {
            CreateMap<Product, ProductViewModel>();
        }
    }
}
