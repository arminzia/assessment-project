﻿using Assessment.WebAPI.Models.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;

namespace Assessment.WebAPI.Models.EntityConfig
{
    public class ProductConfig : IEntityTypeConfiguration<Product>
    {
        public void Configure(EntityTypeBuilder<Product> builder)
        {
            builder.HasKey(x => x.Id);
            builder.Property(x => x.Id).ValueGeneratedOnAdd();

            builder.Property(x => x.Name)
                .IsRequired()
                .HasMaxLength(50);

            builder.Property(x => x.Price)
                .IsRequired()
                .HasColumnType("decimal(18,4)");

            builder.HasData(
                new Product
                {
                    Id = Guid.NewGuid(),
                    Name = "Shield",
                    Price = 1149m
                },
                new Product
                {
                    Id = Guid.NewGuid(),
                    Name = "Helmet",
                    Price = 999m
                },
                new Product
                {
                    Id = Guid.NewGuid(),
                    Name = "Sword",
                    Price = 899m
                },
                new Product
                {
                    Id = Guid.NewGuid(),
                    Name = "Axe",
                    Price = 799m
                },
                new Product
                {
                    Id = Guid.NewGuid(),
                    Name = "Knife",
                    Price = 349m
                },
                new Product
                {
                    Id = Guid.NewGuid(),
                    Name = "Bow",
                    Price = 649m
                },
                new Product
                {
                    Id = Guid.NewGuid(),
                    Name = "Gold Coin",
                    Price = 249m
                },
                new Product
                {
                    Id = Guid.NewGuid(),
                    Name = "Platinum Coin",
                    Price = 399m
                },
                new Product
                {
                    Id = Guid.NewGuid(),
                    Name = "Silver Coin",
                    Price = 50m
                }
            );
        }
    }
}
