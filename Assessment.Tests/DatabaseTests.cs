using Assessment.WebAPI.Models;
using Assessment.WebAPI.Models.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Assessment.Tests
{
    [TestClass]
    public class DatabaseTests
    {
        [TestMethod]
        public void InsertProduct()
        {
            var options = new DbContextOptionsBuilder<AssessmentContext>()
                .UseInMemoryDatabase(databaseName: "insert_product_database")
                .Options;

            // run the test against one instance of the context
            using (var context = new AssessmentContext(options))
            {
                var product = new Product
                {
                    Name = "My product",
                    Price = 9.99m
                };

                context.Products.Add(product);
                context.SaveChanges();
            }

            // use a separate instance of the context to verify correct data was saved to database
            using (var context = new AssessmentContext(options))
            {
                Assert.AreEqual(1, context.Products.Count());

                var product = context.Products.Single();

                Assert.AreEqual("My product", product.Name);
                Assert.AreEqual(9.99m, product.Price);
            }
        }

        [TestMethod]
        public void InsertProductAndDeleteIt()
        {
            var options = new DbContextOptionsBuilder<AssessmentContext>()
                .UseInMemoryDatabase(databaseName: "insert_product_and_delete_it_database")
                .Options;

            // run the test against one instance of the context
            using (var context = new AssessmentContext(options))
            {
                var product = new Product
                {
                    Name = "My product",
                    Price = 9.99m
                };

                context.Products.Add(product);
                context.SaveChanges();
            }

            // use a separate instance of the context to verify correct data was saved to database
            using (var context = new AssessmentContext(options))
            {
                context.Products.Remove(context.Products.First());
                context.SaveChanges();

                Assert.AreEqual(0, context.Products.Count());
            }
        }

        [TestMethod]
        public void InsertPackage()
        {
            var options = new DbContextOptionsBuilder<AssessmentContext>()
                .UseInMemoryDatabase(databaseName: "insert_package_database")
                .Options;

            var packageId = Guid.NewGuid();

            using (var context = new AssessmentContext(options))
            {
                var sword = new Product
                {
                    Name = "Sword",
                    Price = 399m
                };

                var shield = new Product
                {
                    Name = "Shield",
                    Price = 1299m
                };

                context.Products.Add(sword);
                context.Products.Add(shield);
                context.SaveChanges();

                var package = new Package
                {
                    Id = packageId,
                    Name = "Warrior Pack",
                    Description = "Begin your journey as a warrior",
                    Products = new List<PackageItem>
                    {
                        new PackageItem
                        {
                            ProductId = sword.Id
                        },
                        new PackageItem
                        {
                            ProductId = shield.Id
                        }
                    }
                };

                context.Packages.Add(package);
                context.SaveChanges();
            }

            // use a separate instance of the context to verify correct data was saved to database
            using (var context = new AssessmentContext(options))
            {
                var products = context.Products.ToList();
                Assert.AreEqual(2, products.Count);

                var package = context.Packages
                    .Where(x => x.Id == packageId)
                    .Include(x => x.Products)
                    .ThenInclude(x => x.Product)
                    .SingleOrDefault();

                Assert.IsNotNull(package);

                Assert.AreEqual("Warrior Pack", package.Name);
                Assert.AreEqual("Begin your journey as a warrior", package.Description);
                Assert.AreEqual(2, package.Products.Count);
            }
        }
    }
}
