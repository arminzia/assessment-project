﻿using System.Collections.Generic;

namespace Assessment.WebAPI.Models.ViewModels
{
    public class ExchangeRatesApiResponse
    {
        public string Base { get; set; }

        public string Date { get; set; }

        public Dictionary<string,decimal> Rates { get; set; }

        public ExchangeRatesApiResponse()
        {
            Rates = new Dictionary<string, decimal>();
        }
    }
}
