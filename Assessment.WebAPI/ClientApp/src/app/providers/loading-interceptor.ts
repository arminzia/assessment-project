import { Injectable } from "@angular/core";
import {
  HttpRequest,
  HttpResponse,
  HttpHandler,
  HttpInterceptor
} from "@angular/common/http";

import { tap, catchError } from "rxjs/operators";
import { NgProgress, NgProgressRef } from "@ngx-progressbar/core";

@Injectable()
export class LoadingInterceptor implements HttpInterceptor {
  progressRef: NgProgressRef;
  private totalRequests = 0;

  constructor(private ngProgressBar: NgProgress) {
    this.progressRef = ngProgressBar.ref();
  }

  intercept(request: HttpRequest<any>, next: HttpHandler) {
    this.totalRequests++;
    this.progressRef.start();

    return next.handle(request).pipe(
      tap(res => {
        if (res instanceof HttpResponse) {
          this.decreaseRequests();
        }
      }),
      catchError(err => {
        this.decreaseRequests();
        throw err;
      })
    );
  }

  private decreaseRequests() {
    this.totalRequests--;
    if (this.totalRequests === 0) {
      this.progressRef.complete();
    }
  }
}
